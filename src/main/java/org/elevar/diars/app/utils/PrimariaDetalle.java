package org.elevar.diars.app.utils;

import java.io.Serializable;

import org.elevar.diars.app.models.entity.FacturaEntity;
import org.elevar.diars.app.models.entity.ProductoEntity;

public class PrimariaDetalle implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private FacturaEntity factura;
	private ProductoEntity producto;
	public FacturaEntity getFactura() {
		return factura;
	}
	public void setFactura(FacturaEntity factura) {
		this.factura = factura;
	}
	public ProductoEntity getProducto() {
		return producto;
	}
	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

}
