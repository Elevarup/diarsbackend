package org.elevar.diars.app.utils;

import org.springframework.http.HttpStatus;

public class ResponseBusiness {
	private boolean status;
	private Object entidad;
	private String message;
	private HttpStatus code;
	
	
	
	public ResponseBusiness() {
		this.status=true;
		this.entidad=null;
		this.message=MessageResponse.OK;
		this.code=HttpStatus.OK;
	}
	///////
	


	public HttpStatus getCode() {
		return code;
	}



	public void setCode(HttpStatus code) {
		this.code = code;
	}



	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Object getEntidad() {
		return entidad;
	}

	public void setEntidad(Object entidad) {
		this.entidad = entidad;
	}
}
