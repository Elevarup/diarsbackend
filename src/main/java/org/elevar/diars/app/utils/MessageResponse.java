package org.elevar.diars.app.utils;

public class MessageResponse {

	public static final String OK="Éxito";
	public static final String CREATE_RESOURCE="Recurso creado exitosamente";
	
	//////
	public static final String NOT_FOUND="Recurso no encontrado";
	///////
	public static final String INTERNAL_SERVER="Error interno del servidor";
}
