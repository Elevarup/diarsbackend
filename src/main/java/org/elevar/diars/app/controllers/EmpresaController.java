package org.elevar.diars.app.controllers;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.elevar.diars.app.models.services.interfaces.IEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/lunesdiars/empresas")
public class EmpresaController {

	@Autowired
	private IEmpresaService servicio;

	/// LISTA DE LAS EMPRESAS SIN FILTROS
	@GetMapping
	public List<EmpresaEntity> index() {
		return this.servicio.findAll();
	}

	/// LISTA DE LAS EMPRESAS POR RUC OR RAZON SOCIAL
	@PostMapping("/parametros")
	public ResponseEntity<?> showRucOrRazonSocial(@RequestBody EmpresaEntity empresa) {
		List<EmpresaEntity> listaEmpresas = new ArrayList<EmpresaEntity>();
		Map<String, Object> response = new CommonController<EmpresaEntity>().camposNulosEmpresa(empresa);
		
		if(new CommonController<EmpresaEntity>().allEmptyEmpresa(empresa)) {
				response.put("mensaje","Todos los campos están vacios");
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}

		try {	
			if (response.get("validacion").equals("false") == false) {
				listaEmpresas = this.servicio.findByRucOrRazonSocial(empresa.getRuc(), empresa.getRazonSocial());
			}

		} catch (DataAccessException e) {
			return new CommonController<EmpresaEntity>().excepcionInternalServe("select", e);
		}

		if (listaEmpresas.isEmpty()) {
			response.put("mensaje", "Lista de empresas vacía");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<EmpresaEntity>>(listaEmpresas, HttpStatus.OK);
	}

	////OBTENER UNA EMPRESA POR SU CODIGO
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable(name = "id") Integer codigo) {
		EmpresaEntity empresa = null;
		try {
			empresa=this.servicio.findById(codigo);
		} catch (DataAccessException e) {
			return new CommonController<EmpresaEntity>().excepcionInternalServe("select", e);
		}
		
		if(empresa==null) return new CommonController<EmpresaEntity>().notFound(codigo.toString(), "empresa");
		
		return new CommonController<EmpresaEntity>().success(HttpStatus.OK, empresa, "empresa");
	}
	@PostMapping
	public ResponseEntity<?> create(@RequestBody EmpresaEntity empresa){
		EmpresaEntity empresaNew=null;
		try {
			empresa=this.servicio.save(empresa);
		} catch (DataAccessException e) {
			return new CommonController<EmpresaEntity>().excepcionInternalServe("create", e);
		}
		
		return new CommonController<EmpresaEntity>().success(HttpStatus.CREATED, empresaNew, "empresa");
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<?> update(@PathVariable Integer codigo,@RequestBody EmpresaEntity empresa){
		EmpresaEntity empresaActual=this.servicio.findById(codigo);
		EmpresaEntity empresaNew=null;
		
		if(empresaActual==null) return new CommonController<EmpresaEntity>().notFound(codigo.toString(), "empresa");
		
		try {
			empresaActual.setRazonSocial(empresa.getRazonSocial());
			empresaActual.setCorreo(empresa.getCorreo());
			empresaActual.setDireccion(empresa.getDireccion());
			empresaActual.setRuc(empresa.getRuc());
			empresaActual.setTelefono(empresa.getTelefono());
			empresaActual.setTipoCliente(empresa.getTipoCliente());
			
			empresaNew=this.servicio.save(empresaActual);
			
		} catch (DataAccessException e) {
			return new CommonController<EmpresaEntity>().excepcionInternalServe("update", e);
		}
		return new CommonController<EmpresaEntity>().success(HttpStatus.CREATED, empresaNew, "empresa");
	}
}
