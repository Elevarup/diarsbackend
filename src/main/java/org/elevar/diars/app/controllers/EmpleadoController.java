package org.elevar.diars.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.business.EmpleadoBusiness;
import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.services.interfaces.IEmpleadoService;
import org.elevar.diars.app.utils.ResponseBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/lunesdiars/empleados")
public class EmpleadoController {

	@Autowired
	private IEmpleadoService servicio;
	
	@Autowired
	private EmpleadoBusiness empleadoBusiness;

	/// LISTA DE LAS EMPLEADOS SIN FILTROS
	@GetMapping
	public ResponseEntity<?> index() {
		ResponseBusiness responseBusiness = empleadoBusiness.listarEmpleadosActivos();
		return new ResponseEntity<ResponseBusiness>(responseBusiness,responseBusiness.getCode());
	}

	/// LISTA DE LAS EMPLEADOS POR RUC OR RAZON SOCIAL
	@PostMapping("/parametros")
	public ResponseEntity<?> showCodOrNomOrAPOrAMOrNaci(@RequestBody EmpleadoEntity empleado) {
		List<EmpleadoEntity> listaEmpleados = new ArrayList<EmpleadoEntity>();
		Map<String, Object> response = new HashMap<String,Object>();
		
		if((new CommonController<EmpleadoEntity>().allEmptyEmpleado(empleado))) {
				response.put("mensaje","Todos los campos están vacios");
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}

		try {
				listaEmpleados = this.servicio.findByCodigoOrNombreOrApellidoPaternoOrApellidoMaternoOrDocumento(empleado.getCodigo(), empleado.getNombre(),empleado.getApellidoPaterno(), empleado.getApellidoMaterno(), empleado.getDocumento());

		} catch (DataAccessException e) {
			return new CommonController<EmpleadoEntity>().excepcionInternalServe("select", e);
		}

		if (listaEmpleados.isEmpty()) {
			response.put("mensaje", "Lista de empleados vacía");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<EmpleadoEntity>>(listaEmpleados, HttpStatus.OK);
	}

	////OBTENER UNA EMPLEADOS POR SU CODIGO
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@Valid @PathVariable(name = "id") Integer codigo) {
		ResponseBusiness responseBusiness = empleadoBusiness.getEmpleadoById(codigo);
		return new ResponseEntity<ResponseBusiness>(responseBusiness,responseBusiness.getCode());
	}
	@PostMapping
	public ResponseEntity<?> create(@RequestBody EmpleadoEntity empleado){
		EmpleadoEntity empleadoNew=null;
		try {
			empleado=this.servicio.save(empleado);
		} catch (DataAccessException e) {
			return new CommonController<EmpleadoEntity>().excepcionInternalServe("create", e);
		}
		
		return new CommonController<EmpleadoEntity>().success(HttpStatus.CREATED, empleadoNew, "empleado");
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<?> update(@PathVariable Integer codigo,@RequestBody EmpleadoEntity empleado){
		EmpleadoEntity empleadoActual=this.servicio.findById(codigo);
		EmpleadoEntity empleadoNew=null;
		
		if(empleadoActual==null) return new CommonController<EmpleadoEntity>().notFound(codigo.toString(), "empleado");
		
		try {
			empleadoActual.setNombre(empleado.getNombre());
			empleadoActual.setApellidoPaterno(empleado.getApellidoPaterno());
			empleadoActual.setApellidoMaterno(empleado.getApellidoMaterno());
			empleadoActual.setDocumento(empleado.getDocumento());
			
			empleadoNew=this.servicio.save(empleadoActual);
			
		} catch (DataAccessException e) {
			return new CommonController<EmpleadoEntity>().excepcionInternalServe("update", e);
		}
		return new CommonController<EmpleadoEntity>().success(HttpStatus.CREATED, empleadoNew, "empleado");
	}
	////Eliminar un empleado
	@DeleteMapping("/{codigo}")
	public ResponseEntity<?> delete(@PathVariable(name = "codigo")Integer codigo){
		try {
			this.servicio.deleteById(codigo);
		} catch (DataAccessException e) {
			return new CommonController<EmpleadoEntity>().excepcionInternalServe("delete", e);
		}
			return new CommonController<EmpleadoEntity>().successDelete("empleado", codigo.toString());
	}
}