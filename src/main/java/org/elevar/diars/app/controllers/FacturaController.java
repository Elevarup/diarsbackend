package org.elevar.diars.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.entity.FacturaEntity;
import org.elevar.diars.app.models.services.interfaces.IFacturaService;
import org.elevar.diars.app.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/lunesdiars/facturas")
public class FacturaController {

	@Autowired
	private IFacturaService servicio;
	
	@GetMapping
	public List<FacturaEntity> index(){
		return this.servicio.findAll();		
	}
	
	/// LISTA DE LAS EMPLEADOS POR RUC OR RAZON SOCIAL
	@PostMapping("/parametros")
	public ResponseEntity<?> showCodOrNomOrAPOrAMOrNaci(@RequestBody FacturaEntity factura) {
		List<FacturaEntity> listaFacturas = new ArrayList<FacturaEntity>();
		Map<String, Object> response = new HashMap<String,Object>();
		
		if(!(new Utils().allEmptyFactura(factura))) {
				response.put("mensaje","Todos los campos están vacios");
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}

		try {
				listaFacturas = this.servicio.findByCodigoOrFechaOrEmpresaOrEmpleado(factura.getCodigo(), factura.getFecha(), factura.getEmpresa(), factura.getEmpleado());

		} catch (DataAccessException e) {
			return new CommonController<FacturaEntity>().excepcionInternalServe("select", e);
		}

		if (listaFacturas.isEmpty()) {
			response.put("mensaje", "Lista de facturas vacía");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<FacturaEntity>>(listaFacturas, HttpStatus.OK);
	}

	////OBTENER UNA EMPLEADOS POR SU CODIGO
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable(name = "id") Integer codigo) {
		FacturaEntity factura = null;
		try {
			factura=this.servicio.findById(codigo);
		} catch (DataAccessException e) {
			return new CommonController<FacturaEntity>().excepcionInternalServe("select", e);
		}
		
		if(factura==null) return new CommonController<FacturaEntity>().notFound(codigo.toString(), "factura");
		
		return new CommonController<FacturaEntity>().success(HttpStatus.OK, factura, "factura");
	}
	@PostMapping
	public ResponseEntity<?> create(@RequestBody FacturaEntity factura){
		FacturaEntity facturaNew=null;
		try {
			factura=this.servicio.save(factura);
		} catch (DataAccessException e) {
			return new CommonController<FacturaEntity>().excepcionInternalServe("create", e);
		}
		
		return new CommonController<FacturaEntity>().success(HttpStatus.CREATED, facturaNew, "factura");
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<?> update(@PathVariable Integer codigo,@RequestBody FacturaEntity factura){
		FacturaEntity facturaActual=this.servicio.findById(codigo);
		FacturaEntity facturaNew=null;
		
		if(facturaActual==null) return new CommonController<FacturaEntity>().notFound(codigo.toString(), "factura");
		
		try {
			facturaActual.setFecha(factura.getFecha());
			facturaActual.setEmpleado(factura.getEmpleado());
			facturaActual.setEmpresa(factura.getEmpresa());
			facturaActual.setMonto(factura.getMonto());
			facturaActual.setIgv(factura.getIgv());
			
			facturaNew=this.servicio.save(facturaActual);
			
		} catch (DataAccessException e) {
			return new CommonController<FacturaEntity>().excepcionInternalServe("update", e);
		}
		return new CommonController<FacturaEntity>().success(HttpStatus.CREATED, facturaNew, "factura");
	}
	
}
