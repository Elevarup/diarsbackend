package org.elevar.diars.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.entity.ProductoEntity;
import org.elevar.diars.app.models.services.interfaces.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/lunesdiars/productos")
public class ProductoController {

	@Autowired
	private IProductoService servicio;
	
	///lista de productos
	@GetMapping
	public List<ProductoEntity> index(){
		return this.servicio.findAll();
	}
	
	//Encontrar producto por su codigo
	@GetMapping("/{codigo}")
	public ResponseEntity<?> showId(@PathVariable(name = "codigo") Integer codigo){
		ProductoEntity producto=null;
		
		try {
			producto=this.servicio.findByCodigo(codigo);
		} catch (DataAccessException e) {
			return new CommonController<ProductoEntity>().excepcionInternalServe("select", e);
		}
		
		if(producto==null) return new CommonController<ProductoEntity>().notFound(codigo.toString(), "producto");
		
		return new CommonController<ProductoEntity>().success(HttpStatus.OK, producto, "producto");
	}
	////Registrar un producto
	@PostMapping
	public ResponseEntity<?> create(@RequestBody ProductoEntity producto){
		ProductoEntity productoNew=null;
		
		try {
			productoNew=this.servicio.save(producto);
		} catch (DataAccessException e) {
			return new CommonController<ProductoEntity>().excepcionInternalServe("create", e);
		}
		
		return new CommonController<ProductoEntity>().success(HttpStatus.CREATED, productoNew, "producto");
		
	}
	@PostMapping("/parametros")
	public ResponseEntity<?> showByNombre(@RequestBody ProductoEntity producto){
		
		List<ProductoEntity> listaProductos=new ArrayList<ProductoEntity>();
		Map<String, Object> response = new HashMap<String, Object>();
		if(producto.getCodigo()==null && producto.getNombre()==null) {
			response.put("mensaje", "campos vacío");
			return new ResponseEntity<Map<String, Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			listaProductos=this.servicio.findByCodigoOrNombre(producto.getCodigo(),producto.getNombre());
		} catch (DataAccessException e) {
			return new CommonController<ProductoEntity>().excepcionInternalServe("select", e);
		}
		
		if(listaProductos.isEmpty()) {
			response.put("mensaje", "No se encontraron registros con ese nombre");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<ProductoEntity>>(listaProductos,HttpStatus.OK);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<?> update(@PathVariable(name = "codigo") Integer codigo,@RequestBody ProductoEntity producto){
		ProductoEntity productoNew= null;
		ProductoEntity productoActual=this.servicio.findByCodigo(codigo);
		
		if(productoActual==null) return new CommonController<ProductoEntity>().notFound(codigo.toString(), "producto");
		
		try {
			productoActual.setNombre(producto.getNombre());
			productoActual.setPrecio(producto.getPrecio());
			productoActual.setStock(producto.getStock());
			productoActual.setCategoria(producto.getCategoria());
			
			productoNew= this.servicio.save(productoActual);
		} catch (DataAccessException e) {
			return new CommonController<ProductoEntity>().excepcionInternalServe("update", e);
		}
		
		return new CommonController<ProductoEntity>().success(HttpStatus.CREATED, productoNew, "producto");
	}
	
	@DeleteMapping("/{codigo}")
	public ResponseEntity<?> deleteByCodigo(@PathVariable(name = "codigo") Integer codigo){
		try {
			this.servicio.deleteByCodigo(codigo);
		} catch (DataAccessException e) {
			return new CommonController<ProductoEntity>().excepcionInternalServe("delete", e);
		}
		
		return new CommonController<ProductoEntity>().successDelete("producto", codigo.toString());
	}
}
