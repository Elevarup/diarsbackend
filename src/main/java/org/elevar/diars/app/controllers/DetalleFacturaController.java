package org.elevar.diars.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.entity.DetalleFacturaEntity;
import org.elevar.diars.app.models.services.interfaces.IDetalleFacturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/lunesdiars/detalles")
public class DetalleFacturaController {
	
	@Autowired
	private IDetalleFacturaService servicio;

	
	
	@GetMapping
	public List<DetalleFacturaEntity> index(){
		return this.servicio.findAll();		
	}
	
	/// LISTA DE LAS EMPLEADOS POR RUC OR RAZON SOCIAL
	@PostMapping("/parametros")
	public ResponseEntity<?> showCodOrNomOrAPOrAMOrNaci(@RequestBody DetalleFacturaEntity detalle) {
		List<DetalleFacturaEntity> listaDetalleFacturas = new ArrayList<DetalleFacturaEntity>();
		Map<String, Object> response = new HashMap<String,Object>();
		
		if(detalle.getFactura()==null && detalle.getProducto()==null) {
				response.put("mensaje","Todos los campos están vacios");
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}

		try {
				listaDetalleFacturas = this.servicio.findByFacturaOrProducto(detalle.getFactura(), detalle.getProducto());

		} catch (DataAccessException e) {
			return new CommonController<DetalleFacturaEntity>().excepcionInternalServe("select", e);
		}

		if (listaDetalleFacturas.isEmpty()) {
			response.put("mensaje", "Lista de detalles vacía");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<DetalleFacturaEntity>>(listaDetalleFacturas, HttpStatus.OK);
	}

	////OBTENER UNA EMPLEADOS POR SU CODIGO
	@GetMapping("/{id}")
	public ResponseEntity<?> show(@PathVariable(name = "id") Integer codigo) {
		DetalleFacturaEntity detalle = null;
		try {
			detalle=this.servicio.findById(codigo);
		} catch (DataAccessException e) {
			return new CommonController<DetalleFacturaEntity>().excepcionInternalServe("select", e);
		}
		
		if(detalle==null) return new CommonController<DetalleFacturaEntity>().notFound(codigo.toString(), "detalle");
		
		return new CommonController<DetalleFacturaEntity>().success(HttpStatus.OK, detalle, "detalle");
	}
	@PostMapping
	public ResponseEntity<?> create(@RequestBody DetalleFacturaEntity detalle){
		DetalleFacturaEntity detalleNew=null;
		try {
			detalle=this.servicio.save(detalle);
		} catch (DataAccessException e) {
			return new CommonController<DetalleFacturaEntity>().excepcionInternalServe("create", e);
		}
		
		return new CommonController<DetalleFacturaEntity>().success(HttpStatus.CREATED, detalleNew, "detalle");
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<?> update(@PathVariable Integer codigo,@RequestBody DetalleFacturaEntity detalle){
		DetalleFacturaEntity detalleActual=this.servicio.findById(codigo);
		DetalleFacturaEntity detalleNew=null;
		
		if(detalleActual==null) return new CommonController<DetalleFacturaEntity>().notFound(codigo.toString(), "detalle");
		
		try {
			detalleActual.setCantidad(detalle.getCantidad());
			
			detalleNew=this.servicio.save(detalleActual);
			
		} catch (DataAccessException e) {
			return new CommonController<DetalleFacturaEntity>().excepcionInternalServe("update", e);
		}
		return new CommonController<DetalleFacturaEntity>().success(HttpStatus.CREATED, detalleNew, "detalle");
	}
}
