package org.elevar.diars.app.controllers;

import java.util.ArrayList;

import java.util.List;
import java.util.Map;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.entity.PersonaEntity;
import org.elevar.diars.app.models.services.interfaces.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/lunesdiars")
public class PersonaController {

	@Autowired
	@Qualifier("personaService")
	private IPersonaService personaServicio;

	@GetMapping("/personas")
	public List<PersonaEntity> index() {
		return this.personaServicio.findAll();
	}

	@PostMapping("/personas/parametros")
	public ResponseEntity<?> showCodigoNombreAPaterAMater(@RequestBody PersonaEntity persona) {
		List<PersonaEntity> listaPersonas = new ArrayList<PersonaEntity>();
		Map<String, Object> response = new CommonController<Map<String, Object>>().camposNulosPersona(persona);
		if(persona.getNombre()==null) persona.setNombre("");
		
		if(new CommonController<PersonaEntity>().allEmptyPersona(persona)) {
			response.put("mensaje", "Todos los campos están vacíos");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		try {

			if (response.get("validacion").equals("false")==false) {
				listaPersonas = this.personaServicio.findByNombreOrApellidoPaternoOrApellidoMaterno(
						 persona.getNombre(), persona.getApellidoPaterno(),
						persona.getApellidoMaterno());
			}
		} catch (DataAccessException e) {
			return new CommonController<PersonaEntity>().excepcionInternalServe("select", e);
		}
		if (listaPersonas.isEmpty()) {
			response.put("mensaje", "No existe elementos con los datos ingresados");
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<PersonaEntity>>(listaPersonas, HttpStatus.OK);
	}

	@GetMapping("/personas/{id}")
	public ResponseEntity<?> show(@PathVariable Integer id) {
		PersonaEntity persona = null;

		try {
			persona = this.personaServicio.findByOne(id);
		} catch (DataAccessException e) {
			return new CommonController<PersonaEntity>().excepcionInternalServe("select", e);
		}

		if (persona == null)
			return new CommonController<PersonaEntity>().notFound(id.toString(), "Persona");
		System.out.println("Persona: ");
		return new CommonController<PersonaEntity>().success(HttpStatus.OK, persona,"persona");
	}

	@PostMapping("/personas")
	public ResponseEntity<?> create(@RequestBody PersonaEntity persona) {
		PersonaEntity personaNueva = null;
		try {
			personaNueva = this.personaServicio.save(persona);
		} catch (DataAccessException e) {
			return new CommonController<PersonaEntity>().excepcionInternalServe("create", e);
		}
		return new CommonController<PersonaEntity>().success(HttpStatus.CREATED, personaNueva, "persona");
	}
	
	@PutMapping("/personas/{codigo}")
	public ResponseEntity<?> edit(@PathVariable Integer codigo,@RequestBody PersonaEntity persona){
		PersonaEntity personaActual = this.personaServicio.findByOne(codigo);
		PersonaEntity personaNueva = null;
		
		if(personaActual==null) {
			return new CommonController<PersonaEntity>().notFound(codigo.toString(), "persona");
		}
		
		personaActual.setApellidoPaterno(persona.getApellidoPaterno());
		try {
			personaActual.setNombre(persona.getNombre());
			personaActual.setApellidoMaterno(persona.getApellidoMaterno());
			personaActual.setCorreo(persona.getCorreo());
			personaActual.setTelefono(persona.getTelefono());
			personaActual.setTipoCliente(persona.getTipoCliente());
			personaActual.setDireccion(persona.getDireccion());
			
			personaNueva=this.personaServicio.save(personaActual);
			
		} catch (DataAccessException e) {
			return new CommonController<PersonaEntity>().excepcionInternalServe("edit", e);
		}
		
		return new CommonController<PersonaEntity>().success(HttpStatus.CREATED, personaNueva, "persona");
	}
	
	@DeleteMapping("/personas/{codigo}")
	public ResponseEntity<?> delete(@PathVariable Integer codigo){
		try {
			this.personaServicio.deleteByOne(codigo);
		} catch (DataAccessException e) {
			return new CommonController<PersonaEntity>().excepcionInternalServe("DELETE", e);
		}
		return new CommonController<PersonaEntity>().successDelete("Persona", codigo.toString());
	}
}
