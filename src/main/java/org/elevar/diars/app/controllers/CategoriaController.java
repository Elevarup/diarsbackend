package org.elevar.diars.app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elevar.diars.app.common.CommonController;
import org.elevar.diars.app.models.entity.CategoriaEntity;
import org.elevar.diars.app.models.services.interfaces.ICategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/lunesdiars/categorias")
public class CategoriaController {

	@Autowired
	private ICategoriaService servicio;
	
	@GetMapping
	public List<CategoriaEntity> index(){
		return this.servicio.findAll();
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<?> show(@PathVariable(name = "codigo") Integer codigo){
		CategoriaEntity categoria=null;
		
		try {
			categoria=this.servicio.findById(codigo);
		} catch (DataAccessException e) {
			 return new CommonController<CategoriaEntity>().excepcionInternalServe("select", e);
		}
		
		if(categoria==null) return new CommonController<CategoriaEntity>().notFound(codigo.toString(), "categoria");
		
		return new CommonController<CategoriaEntity>().success(HttpStatus.OK, categoria, "categoria");
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody CategoriaEntity categoria){
		CategoriaEntity categoriaNew = null;
		try {
			categoriaNew = this.servicio.save(categoria);
		} catch (DataAccessException e) {
			return new CommonController<CategoriaEntity>().excepcionInternalServe("create", e);
		}
	
		return new CommonController<CategoriaEntity>().success(HttpStatus.CREATED, categoriaNew, "categoria");
	}	
	////Buscar categoria por nombre
	@PostMapping("/parametros")
	public ResponseEntity<?> findByNombre(@RequestBody CategoriaEntity categoria){
		List<CategoriaEntity> listaCategoria=new ArrayList<CategoriaEntity>();
		Map<String,Object> response = new HashMap<String, Object>();
		if(categoria.getNombre()==null) {
			response.put("mensaje", "campo vacío");
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			listaCategoria=this.servicio.findByNombre(categoria.getNombre());
		} catch (DataAccessException e) {
			return new CommonController<CategoriaEntity>().excepcionInternalServe("select", e);
		}
		
		return new ResponseEntity<List<CategoriaEntity>>(listaCategoria,HttpStatus.OK);
	} 
	
	
	@PutMapping("/{codigo}")
	public ResponseEntity<?> update(@PathVariable(name = "codigo") Integer codigo,@RequestBody CategoriaEntity categoria){
		CategoriaEntity categoriaNew = null;
		CategoriaEntity categoriaActual=this.servicio.findById(codigo);
		
		if(categoriaActual==null) return new CommonController<CategoriaEntity>().notFound(codigo.toString(), "categoria");
		
		try {
			categoriaActual.setNombre(categoria.getNombre());
			
			categoriaNew=this.servicio.save(categoriaActual);
		} catch (DataAccessException e) {
			return new CommonController<CategoriaEntity>().excepcionInternalServe("update", e);
		}
		return new CommonController<CategoriaEntity>().success(HttpStatus.CREATED, categoriaNew, "categoria");
	}
	@DeleteMapping("/{codigo}")
	public ResponseEntity<?> delete(@PathVariable(name = "codigo") Integer codigo){
		try {
			this.servicio.deleteByCodigo(codigo);
		} catch (DataAccessException e) {
			return new CommonController<CategoriaEntity>().excepcionInternalServe("delete", e);
		}
		return new CommonController<CategoriaEntity>().successDelete("categoria", codigo.toString());
	}
}
