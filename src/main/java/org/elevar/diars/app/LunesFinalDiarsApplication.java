package org.elevar.diars.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LunesFinalDiarsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LunesFinalDiarsApplication.class, args);
	}

}
