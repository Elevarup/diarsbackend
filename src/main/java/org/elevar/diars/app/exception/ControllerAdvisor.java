package org.elevar.diars.app.exception;

import org.elevar.diars.app.utils.ResponseBusiness;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerAdvisor  extends ResponseEntityExceptionHandler{
	
	private ResponseBusiness response;

	@ExceptionHandler(NumberFormatException.class)
	public ResponseEntity<?> NumberFormatException(){
		System.out.println("ControllerAdvice");
		response = new ResponseBusiness();
		response.setMessage("Parámetro no válido");
		response.setStatus(false);
		response.setCode(HttpStatus.BAD_REQUEST);

		return new ResponseEntity<ResponseBusiness>(response,response.getCode());
	}
}
