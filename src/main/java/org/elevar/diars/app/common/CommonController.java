package org.elevar.diars.app.common;

import java.util.HashMap;
import java.util.Map;

import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.elevar.diars.app.models.entity.PersonaEntity;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CommonController<T> {

	private Map<String, Object> response = new HashMap<>();

	public ResponseEntity<?> excepcionInternalServe(String ddl, DataAccessException e) {
		response.put("mensaje", "No se puede realizar la operacion (".concat(ddl).concat("), en la base de datos"));
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	public ResponseEntity<?> success(HttpStatus Status, T entity, String nombreEntidad) {
		response.put("mensaje", "Éxito");
		response.put(nombreEntidad, entity);

		return new ResponseEntity<Map<String, Object>>(response, Status);
	}

	public ResponseEntity<?> successDelete(String nombreEntidad,String codigo){
			response.put("mensaje",nombreEntidad.concat(" con ID: ").concat(codigo).concat(", eliminado correctamente"));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}
	public ResponseEntity<?> success(HttpStatus Status, T entity) {
		response.put("mensaje", "Éxito");
		response.put("Entity", entity);

		return new ResponseEntity<Map<String, Object>>(response, Status);
	}

	public ResponseEntity<?> notFound(String codigo, String nombreEntidad) {
		this.response.put("mensaje", nombreEntidad.concat(" con ID: ").concat(codigo).concat(", no existe en la bd"));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
	}

	public Map<String, Object> camposNulosPersona(PersonaEntity persona) {

		boolean validacion = false;


		if (persona.getApellidoMaterno()== null) {
			response.put("Apellido materno", "nulo");
			validacion = true;
		}

		if (persona.getNombre()==null) {
			response.put("Nombre", "nulo");
			validacion = true;
		}
		if (persona.getApellidoPaterno() == null) {
			response.put("Apellido paterno", "nulo");
			validacion = true;
		}
		response.put("validacion", validacion);

		return response;
	}
	
	
	
	/////CAMPOS NULOS PARA LA EMPRESA
	public Map<String, Object> camposNulosEmpresa(EmpresaEntity empresa) {

		boolean validacion = false;


		if (empresa.getRazonSocial()== null) {
			response.put("Razon social", "nulo");
			validacion = true;
		}
		response.put("validacion", validacion);

		return response;
	}
	public boolean allEmptyPersona(PersonaEntity persona) {
			if(persona.getNombre()==null && persona.getApellidoPaterno()==null && persona.getApellidoMaterno()==null) return true;
			return false;
	}
	public boolean allEmptyEmpresa(EmpresaEntity empresa) {
			if( empresa.getRuc()==null && empresa.getRazonSocial()==null) return true;
			return false;
	}
	public boolean allEmptyEmpleado(EmpleadoEntity empleado) {
	
		if(empleado.getCodigo()==null && empleado.getNombre()==null && empleado.getApellidoPaterno()==null && empleado.getApellidoMaterno()==null && empleado.getDocumento()==null) return true;
		return false;
	}
}