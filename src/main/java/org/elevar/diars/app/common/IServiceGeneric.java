package org.elevar.diars.app.common;

import java.util.List;

public interface IServiceGeneric<T,ID> {

	public List<T> findAll();
	public T findById(ID id);
	public T save(T entity);
	public void deleteById(ID id);

}
