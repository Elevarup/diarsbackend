package org.elevar.diars.app.models.services.interfaces;

import java.util.List;

import org.elevar.diars.app.models.entity.CategoriaEntity;

public interface ICategoriaService {

	public List<CategoriaEntity> findAll();
	public CategoriaEntity findById(Integer codigo);
	public CategoriaEntity save(CategoriaEntity categoria);
	public void deleteByCodigo(Integer codigo);

	
	public List<CategoriaEntity> findByNombre(String nombre);
}
