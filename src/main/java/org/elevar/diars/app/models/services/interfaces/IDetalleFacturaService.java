package org.elevar.diars.app.models.services.interfaces;

import java.util.List;

import org.elevar.diars.app.models.entity.DetalleFacturaEntity;
import org.elevar.diars.app.models.entity.FacturaEntity;
import org.elevar.diars.app.models.entity.ProductoEntity;

public interface IDetalleFacturaService {

	public List<DetalleFacturaEntity> findAll();
	public DetalleFacturaEntity findById(Integer codigo);
	public DetalleFacturaEntity save(DetalleFacturaEntity factura);
	public void deleteById(Integer codigo);

	public List<DetalleFacturaEntity> findByFacturaOrProducto(FacturaEntity factura,ProductoEntity producto);
}
