package org.elevar.diars.app.models.entity;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "productos")
public class ProductoEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pro")
	private Integer codigo;
	
	@Column(name = "nom_pro",length = 50,nullable = false)
	private String nombre;
	
	@Column(name = "stock_pro",nullable = false,columnDefinition = "INT DEFAULT 0 ")
	private int stock;
	
	@Column(name = "pre_pro",columnDefinition = "DOUBLE DEFAULT 0.0")
	private double precio;
	
	@ManyToOne
	@JoinColumn(name = "id_cat_pro")
	private CategoriaEntity categoria;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public CategoriaEntity getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaEntity categoria) {
		this.categoria = categoria;
	}
}
