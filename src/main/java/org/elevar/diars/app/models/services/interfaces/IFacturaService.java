package org.elevar.diars.app.models.services.interfaces;

import java.util.Date;
import java.util.List;

import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.elevar.diars.app.models.entity.FacturaEntity;

public interface IFacturaService {

	public List<FacturaEntity> findAll();
	public FacturaEntity findById(Integer codigo);
	public FacturaEntity save(FacturaEntity factura);
	public void deleteById(Integer codigo);
	
	public List<FacturaEntity> findByCodigoOrFechaOrEmpresaOrEmpleado(Integer codigo,Date fecha,EmpresaEntity empresa,EmpleadoEntity empleado);
}
