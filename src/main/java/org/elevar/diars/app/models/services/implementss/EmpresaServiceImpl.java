package org.elevar.diars.app.models.services.implementss;

import java.util.List;

import org.elevar.diars.app.models.dao.IEmpresaDao;
import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.elevar.diars.app.models.services.interfaces.IEmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpresaServiceImpl implements IEmpresaService {
	
	@Autowired
	private IEmpresaDao dao;

	@Override
	public List<EmpresaEntity> findAll() {
		return this.dao.findAll();
	}

	@Override
	public EmpresaEntity findById(Integer codigo) {
		return this.dao.findById(codigo).orElse(null);
	}

	@Override
	public EmpresaEntity save(EmpresaEntity empresa) {
		return this.dao.save(empresa);
	}

	@Override
	public void deleteById(Integer codigo) {
		this.dao.deleteById(codigo);
	}

	@Override
	public List<EmpresaEntity> findByCodigoOrRucOrRazonSocial(Integer codigo,String ruc, String razonSocial) {
		return this.dao.findByCodigoOrRucOrRazonSocial(codigo, ruc, razonSocial);
	}

	@Override
	public List<EmpresaEntity> findByRucOrRazonSocial(String ruc, String razonSocial) {
		return this.dao.findByRucOrRazonSocial(ruc, razonSocial);
	}


}
