package org.elevar.diars.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "empresas")
public class EmpresaEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name = "id_emp",nullable = false)
	private Integer codigo;
	
	@Column(name = "ruc_emp",length = 30,unique = true)
	private String ruc;
	

	@Column(name = "raz_soc_emp",length = 200,nullable = false)
	private String razonSocial;
	
	@Column(name = "tel_emp",length = 15)
	private String telefono;
	
	@Column(name = "dir_emp",length = 100)
	private String direccion;
	

	@Column(name = "cor_emp",length = 100,nullable = false, unique = true)
	private String correo;
	
	@OneToOne
	@JoinColumn(name = "id_tip_cli")
	private TipoClienteEntity tipoCliente;
	
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "empresa")
	@JsonManagedReference
	private List<FacturaEntity> facturas;
	
	
	

	public EmpresaEntity() {
		facturas = new ArrayList<FacturaEntity>();
	}

	public List<FacturaEntity> getFacturas() {
		return facturas;
	}
	
	public void addFactura(FacturaEntity factura) {
		this.facturas.add(factura);
	}

	public void setFacturas(List<FacturaEntity> facturas) {
		this.facturas = facturas;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public TipoClienteEntity getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoClienteEntity tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
}