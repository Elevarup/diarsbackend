package org.elevar.diars.app.models.services.interfaces;

import java.util.List;

import org.elevar.diars.app.models.entity.ProductoEntity;

public interface IProductoService {

	public List<ProductoEntity> findAll();
	public ProductoEntity findByCodigo(Integer codigo);
	public List<ProductoEntity> findByCodigoOrNombre(Integer codigo,String nombre);
	public ProductoEntity save(ProductoEntity producto);
	public void deleteByCodigo(Integer codigo);
}
