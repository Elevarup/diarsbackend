package org.elevar.diars.app.models.dao;

import java.util.Date;
import java.util.List;

import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.elevar.diars.app.models.entity.FacturaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFacturaDao extends JpaRepository<FacturaEntity, Integer>{

	public List<FacturaEntity> findByCodigoOrFechaOrEmpresaOrEmpleado(Integer codigo,Date fecha,EmpresaEntity empresa,EmpleadoEntity empleado);
}
