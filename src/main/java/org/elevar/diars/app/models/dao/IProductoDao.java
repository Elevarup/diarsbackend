package org.elevar.diars.app.models.dao;

import java.util.List;

import org.elevar.diars.app.models.entity.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductoDao  extends JpaRepository<ProductoEntity, Integer>{

	public List<ProductoEntity> findByCodigoOrNombre(Integer codigo,String nombre);
}
