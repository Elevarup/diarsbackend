package org.elevar.diars.app.models.dao;

import java.util.List;


import org.elevar.diars.app.models.entity.PersonaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaDao extends JpaRepository<PersonaEntity, Integer> {

	public List<PersonaEntity> findByCodigoOrNombreOrApellidoPaternoOrApellidoMaterno(Integer codigo,String nombre,String apellidoPaterno,String apellidoMaterno);
	public List<PersonaEntity> findByNombreOrApellidoPaternoOrApellidoMaterno(String nombre,String apellidoPaterno,String apellidoMaterno);
}
