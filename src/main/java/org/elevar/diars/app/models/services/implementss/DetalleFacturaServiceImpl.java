package org.elevar.diars.app.models.services.implementss;

import java.util.List;

import org.elevar.diars.app.models.dao.IDetalleFacturaDao;
import org.elevar.diars.app.models.entity.DetalleFacturaEntity;
import org.elevar.diars.app.models.entity.FacturaEntity;
import org.elevar.diars.app.models.entity.ProductoEntity;
import org.elevar.diars.app.models.services.interfaces.IDetalleFacturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetalleFacturaServiceImpl implements IDetalleFacturaService {

	@Autowired
	private IDetalleFacturaDao dao;
	
	
	@Override
	public List<DetalleFacturaEntity> findAll() {
		return this.dao.findAll();
	}

	@Override
	public DetalleFacturaEntity findById(Integer codigo) {
		return this.dao.findById(codigo).orElse(null);
	}

	@Override
	public DetalleFacturaEntity save(DetalleFacturaEntity factura) {
		return this.dao.save(factura);
	}

	@Override
	public void deleteById(Integer codigo) {
		this.dao.deleteById(codigo);
	}

	@Override
	public List<DetalleFacturaEntity> findByFacturaOrProducto(FacturaEntity factura, ProductoEntity producto) {
		return this.dao.findByFacturaOrProducto(factura, producto);
	}

}
