package org.elevar.diars.app.models.services.implementss;

import java.util.Date;
import java.util.List;

import org.elevar.diars.app.models.dao.IEmpleadoDao;
import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.services.interfaces.IEmpleadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{
	@Autowired
	private IEmpleadoDao dao;
	
	@Override
	public List<EmpleadoEntity> findAll() {
		return this.dao.findAll();
	}

	@Override
	public EmpleadoEntity findById(Integer codigo) {
		return this.dao.findById(codigo).orElse(null);
	}

	@Override
	public EmpleadoEntity save(EmpleadoEntity empleado) {
		return this.dao.save(empleado);
	}

	@Override
	public void deleteById(Integer codigo) {
		this.dao.deleteById(codigo);
	}

	@Override
	public List<EmpleadoEntity> findByCodigoOrNombreOrApellidoPaternoOrApellidoMaternoOrDocumento(Integer codigo,
			String nombre, String apPaterno, String apMaterno, String documento) {
		return this.dao.findByCodigoOrNombreOrApellidoPaternoOrApellidoMaternoOrDocumento(codigo, nombre, apPaterno, apMaterno, documento);
	}


}
