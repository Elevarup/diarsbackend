package org.elevar.diars.app.models.services.implementss;

import java.util.Date;
import java.util.List;

import org.elevar.diars.app.models.dao.IFacturaDao;
import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.elevar.diars.app.models.entity.FacturaEntity;
import org.elevar.diars.app.models.services.interfaces.IFacturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FacturaServiceImpl  implements IFacturaService{

	@Autowired
	private IFacturaDao dao;
	
	@Override
	public List<FacturaEntity> findAll() {
		return this.dao.findAll();
	}

	@Override
	public FacturaEntity findById(Integer codigo) {
		return this.dao.findById(codigo).orElse(null);
	}

	@Override
	public FacturaEntity save(FacturaEntity factura) {
		return this.dao.save(factura);
	}

	@Override
	public void deleteById(Integer codigo) {
		this.dao.deleteById(codigo);
	}

	@Override
	public List<FacturaEntity> findByCodigoOrFechaOrEmpresaOrEmpleado(Integer codigo, Date fecha, EmpresaEntity empresa,
			EmpleadoEntity empleado) {
		return this.dao.findByCodigoOrFechaOrEmpresaOrEmpleado(codigo, fecha, empresa, empleado);
	}

}
