package org.elevar.diars.app.models.services.implementss;

import java.util.List;

import org.elevar.diars.app.models.dao.IProductoDao;
import org.elevar.diars.app.models.entity.ProductoEntity;
import org.elevar.diars.app.models.services.interfaces.IProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoServiceImpl  implements IProductoService{

	@Autowired
	private IProductoDao dao;
	
	@Override
	public List<ProductoEntity> findAll() {
		return this.dao.findAll();
	}

	@Override
	public ProductoEntity findByCodigo(Integer codigo) {
		return this.dao.findById(codigo).orElse(null);
	}

	@Override
	public List<ProductoEntity> findByCodigoOrNombre(Integer codigo,String nombre) {
		return this.dao.findByCodigoOrNombre(codigo,nombre);
	}

	@Override
	public ProductoEntity save(ProductoEntity producto) {
		return this.dao.save(producto);
	}

	@Override
	public void deleteByCodigo(Integer codigo) {
		this.dao.deleteById(codigo);
	}

}
