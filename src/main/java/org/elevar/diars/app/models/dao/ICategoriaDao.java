package org.elevar.diars.app.models.dao;

import java.util.List;

import org.elevar.diars.app.models.entity.CategoriaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoriaDao extends JpaRepository<CategoriaEntity, Integer>{

	public List<CategoriaEntity> findByNombre(String nombre);
}
