package org.elevar.diars.app.models.entity;

import java.io.Serializable;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.elevar.diars.app.utils.PrimariaDetalle;

import com.fasterxml.jackson.annotation.JacksonInject.Value;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "detalle_factura")
@IdClass(value = PrimariaDetalle.class)
public class DetalleFacturaEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
////////////////////	
	@Id
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "fac_det")
	@JsonBackReference
	private FacturaEntity factura;
	
	@Id
	@OneToOne
	@JoinColumn(name = "id_producto",nullable = false)
	private ProductoEntity producto;
	
	@Column(name = "can_det_fac",columnDefinition = "DOUBLE DEFAULT 0")
	private Integer cantidad;
	////Getters and Setters
	public ProductoEntity getProducto() {
		return producto;
	}

	public void setProducto(ProductoEntity producto) {
		this.producto = producto;
	}

	public FacturaEntity getFactura() {
		return factura;
	}

	public void setFactura(FacturaEntity factura) {
		this.factura = factura;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	//////////////////
	//////////////////
}
