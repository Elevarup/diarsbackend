package org.elevar.diars.app.models.services.implementss;

import java.util.List;

import org.elevar.diars.app.models.dao.ICategoriaDao;
import org.elevar.diars.app.models.entity.CategoriaEntity;
import org.elevar.diars.app.models.services.interfaces.ICategoriaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriaServiceImpl implements ICategoriaService{

	@Autowired
	private ICategoriaDao dao;
	
	@Override
	public List<CategoriaEntity> findAll() {
		return this.dao.findAll();
	}

	@Override
	public CategoriaEntity findById(Integer codigo) {
		return this.dao.findById(codigo).orElse(null);
	}

	@Override
	public CategoriaEntity save(CategoriaEntity categoria) {
		return this.dao.save(categoria);
	}

	@Override
	public void deleteByCodigo(Integer codigo) {
		this.dao.deleteById(codigo);
	}

	@Override
	public List<CategoriaEntity> findByNombre(String nombre) {
		return this.dao.findByNombre(nombre);
	}

}
