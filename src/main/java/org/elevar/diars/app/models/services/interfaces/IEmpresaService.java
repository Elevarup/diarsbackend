package org.elevar.diars.app.models.services.interfaces;

import java.util.List;

import org.elevar.diars.app.models.entity.EmpresaEntity;

public interface IEmpresaService {

	public List<EmpresaEntity> findAll();
	public EmpresaEntity findById(Integer codigo);
	public EmpresaEntity save(EmpresaEntity empresa);
	public void deleteById(Integer codigo);
	
	public List<EmpresaEntity> findByCodigoOrRucOrRazonSocial(Integer codigo,String ruc,String razonSocial);
	public List<EmpresaEntity> findByRucOrRazonSocial(String ruc,String razonSocial);
}
