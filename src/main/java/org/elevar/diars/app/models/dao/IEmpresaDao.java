package org.elevar.diars.app.models.dao;

import java.util.List;

import org.elevar.diars.app.models.entity.EmpresaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmpresaDao extends JpaRepository<EmpresaEntity, Integer> {

	public List<EmpresaEntity> findByCodigoOrRucOrRazonSocial(Integer codigo,String ruc,String razonSocial);
	public List<EmpresaEntity> findByRucOrRazonSocial(String ruc,String razonSocial);
}
