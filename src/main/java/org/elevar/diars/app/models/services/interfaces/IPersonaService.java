package org.elevar.diars.app.models.services.interfaces;

import java.util.List;
import org.elevar.diars.app.models.entity.PersonaEntity;

public interface IPersonaService {
	
	public List<PersonaEntity> findAll();
	public PersonaEntity findByOne(Integer id);
	public PersonaEntity save(PersonaEntity persona);
	public void deleteByOne(Integer id);
	public List<PersonaEntity> findByCodigoOrNombreOrApellidoPaternoOrApellidoMaterno(int codigo,String nombre,String apellidoPaterno,String apellidoMaterno);
	public List<PersonaEntity> findByNombreOrApellidoPaternoOrApellidoMaterno(String nombre,String apellidoPaterno,String apellidoMaterno);
}