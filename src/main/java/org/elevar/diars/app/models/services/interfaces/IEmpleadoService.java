package org.elevar.diars.app.models.services.interfaces;

import java.util.Date;
import java.util.List;

import org.elevar.diars.app.models.entity.EmpleadoEntity;

public interface IEmpleadoService {

	public List<EmpleadoEntity> findAll();
	public EmpleadoEntity findById(Integer codigo);
	public EmpleadoEntity save(EmpleadoEntity empresa);
	public void deleteById(Integer codigo);
	
	public List<EmpleadoEntity> findByCodigoOrNombreOrApellidoPaternoOrApellidoMaternoOrDocumento(Integer codigo,String nombre,String apPaterno,String apMaterno,String documento);
}
