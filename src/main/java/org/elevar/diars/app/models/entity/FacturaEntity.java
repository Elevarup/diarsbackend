package org.elevar.diars.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "facturas")
public class FacturaEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fac")
	private Integer codigo;
	
	@Column(name = "fec_fac")
	@Temporal(TemporalType.DATE)
	private Date fecha;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JoinColumn(name = "empresa_fac")
	@JsonBackReference
	private EmpresaEntity empresa;
	
	@Column(name = "mon_fac")
	private double monto;
	
	@Column(name="igv_fac")
	private double igv;
	
	@OneToOne
	@JoinColumn(name="empleado_fac")
	private EmpleadoEntity empleado;
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "factura")
	@JsonManagedReference
	private List<DetalleFacturaEntity> detalles = new ArrayList<DetalleFacturaEntity>();
	
	public List<DetalleFacturaEntity> getDetalles() {
		return detalles;
	}



	public void setDetalles(List<DetalleFacturaEntity> detalles) {
		this.detalles = detalles;
	}



	public Integer getCodigo() {
		return codigo;
	}



	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}



	public Date getFecha() {
		return fecha;
	}



	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}



	public EmpresaEntity getEmpresa() {
		return empresa;
	}



	public void setEmpresa(EmpresaEntity empresa) {
		this.empresa = empresa;
	}



	public double getMonto() {
		return monto;
	}



	public void setMonto(double monto) {
		this.monto = monto;
	}



	public double getIgv() {
		return igv;
	}



	public void setIgv(double igv) {
		this.igv = igv;
	}



	public EmpleadoEntity getEmpleado() {
		return empleado;
	}



	public void setEmpleado(EmpleadoEntity empleado) {
		this.empleado = empleado;
	}

}
