package org.elevar.diars.app.models.services.implementss;

import java.util.List;

import org.elevar.diars.app.models.dao.IPersonaDao;
import org.elevar.diars.app.models.entity.PersonaEntity;
import org.elevar.diars.app.models.services.interfaces.IPersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("personaService")
public class PersonaServiceImpl implements IPersonaService {
	
	@Autowired
	private IPersonaDao personaDao;

	@Override
	public List<PersonaEntity> findAll() {
		return this.personaDao.findAll();
	}

	@Override
	public PersonaEntity findByOne(Integer id) {
		return this.personaDao.findById(id).orElse(null);
	}

	@Override
	public PersonaEntity save(PersonaEntity persona) {
		return this.personaDao.save(persona);
	}

	@Override
	public void deleteByOne(Integer id) {
		this.personaDao.deleteById(id);
	}

	@Override
	public List<PersonaEntity> findByCodigoOrNombreOrApellidoPaternoOrApellidoMaterno(int codigo, String nombre,
			String apellidoPaterno, String apellidoMaterno) {
		return this.personaDao.findByCodigoOrNombreOrApellidoPaternoOrApellidoMaterno(codigo, nombre, apellidoPaterno, apellidoMaterno);
	}

	@Override
	public List<PersonaEntity> findByNombreOrApellidoPaternoOrApellidoMaterno(String nombre, String apellidoPaterno,
			String apellidoMaterno) {
		return this.personaDao.findByNombreOrApellidoPaternoOrApellidoMaterno(nombre, apellidoPaterno, apellidoMaterno);
	}
}
