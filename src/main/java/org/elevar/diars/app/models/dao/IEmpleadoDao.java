package org.elevar.diars.app.models.dao;

import java.util.Date;
import java.util.List;

import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmpleadoDao extends JpaRepository<EmpleadoEntity,Integer>{

	public List<EmpleadoEntity> findByCodigoOrNombreOrApellidoPaternoOrApellidoMaternoOrDocumento(Integer codigo,String nombre, String apPaterno,String apMaterno,String documento);

}
