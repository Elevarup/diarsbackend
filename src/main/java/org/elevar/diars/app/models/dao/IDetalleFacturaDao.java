package org.elevar.diars.app.models.dao;

import java.util.List;

import org.elevar.diars.app.models.entity.DetalleFacturaEntity;
import org.elevar.diars.app.models.entity.FacturaEntity;
import org.elevar.diars.app.models.entity.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDetalleFacturaDao extends JpaRepository<DetalleFacturaEntity, Integer>{
	
	public List<DetalleFacturaEntity> findByFacturaOrProducto(FacturaEntity factura,ProductoEntity producto);
	
	
}
