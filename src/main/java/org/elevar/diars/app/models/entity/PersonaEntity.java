package org.elevar.diars.app.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity(name = "persona")
@Table(name = "personas")


public class PersonaEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY )
	@Column(name = "id_per")
	private Integer codigo;
	
	@Column(name = "nom_per",length = 100)
	private String nombre;
	
	@Column(name = "app_per",length = 100)
	private String apellidoPaterno;
	
	
	@Column(name = "apm_per",length = 100)
	private String apellidoMaterno;
	
	@Column(name = "tel_per",length = 15)
	private String telefono;
	
	@Column(name = "dir_per",nullable = false,length = 200)
	private String direccion;
	
	@Column(name = "cor_per",nullable = false,unique = true,length = 200)
	private String correo;

	@OneToOne
	@JoinColumn(name = "id_tip_cli")
	private TipoClienteEntity tipoCliente;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public TipoClienteEntity getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoClienteEntity tipoCliente) {
		this.tipoCliente = tipoCliente;
	}
}
