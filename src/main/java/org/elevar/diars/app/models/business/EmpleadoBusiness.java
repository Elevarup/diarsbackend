package org.elevar.diars.app.models.business;


import org.elevar.diars.app.models.entity.EmpleadoEntity;
import org.elevar.diars.app.models.services.interfaces.IEmpleadoService;
import org.elevar.diars.app.utils.MessageResponse;
import org.elevar.diars.app.utils.ResponseBusiness;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class EmpleadoBusiness {
	
	@Autowired
	private IEmpleadoService empleadoService;
	
	private ResponseBusiness responseBusiness;
	
	public EmpleadoBusiness() {
		responseBusiness = new ResponseBusiness();
	};
	

	public ResponseBusiness listarEmpleadosActivos(){
		responseBusiness.setEntidad(empleadoService.findAll());
		return responseBusiness;
	}
	
	public ResponseBusiness getEmpleadoById(Integer id) {
		
		ResponseBusiness responseBusiness = new ResponseBusiness();
		EmpleadoEntity empleado = null;
		
		try {
			empleado = empleadoService.findById(id);
		} catch (DataAccessException e) {
			responseBusiness.setStatus(false);
			responseBusiness.setMessage("Error del servidor");
			responseBusiness.setCode(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(empleado  ==null ) {
			responseBusiness.setCode(HttpStatus.NOT_FOUND);
			responseBusiness.setMessage(MessageResponse.NOT_FOUND);
			responseBusiness.setEntidad(null);
			responseBusiness.setStatus(false);
		}
		
		responseBusiness.setEntidad(empleado);
		return responseBusiness;
	}
}
