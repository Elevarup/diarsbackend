INSERT INTO personas(nom_per,app_per,apm_per,tel_per,dir_per,cor_per) VALUES("uno","uno","uno","345624","uno av uno","uno@uno");
INSERT INTO personas(nom_per,app_per,apm_per,tel_per,dir_per,cor_per) VALUES("dos","dos","dos","345624","dos av dos","dos@uno");
INSERT INTO personas(nom_per,app_per,apm_per,tel_per,dir_per,cor_per) VALUES("tres","tres","tres","345624","tres av tres","tres@uno");

INSERT INTO empresas(ruc_emp,raz_soc_emp,tel_emp,dir_emp,cor_emp) VALUES("RUCUNO","UNO","345624","UNO av UNO","UNO@UNO");
INSERT INTO empresas(ruc_emp,raz_soc_emp,tel_emp,dir_emp,cor_emp) VALUES("RUCDOS","DOS","345624","DOS av DOS","DOS@DOS");
INSERT INTO empresas(ruc_emp,raz_soc_emp,tel_emp,dir_emp,cor_emp) VALUES("RUCTRES","TRES","345624","TRES av TRES","TRES@TRES");
INSERT INTO empresas(ruc_emp,raz_soc_emp,tel_emp,dir_emp,cor_emp) VALUES("RUCCUATRO","CUATRO","345624","CUATRO av CUATRO","CUATRO@CUATRO");
INSERT INTO empresas(ruc_emp,raz_soc_emp,tel_emp,dir_emp,cor_emp) VALUES("RUCCINCO","CINCO","345624","CINCO av CINCO","CINCO@CINCO");


INSERT INTO categorias(nom_cat) VALUES ("Pasta");
INSERT INTO categorias(nom_cat) VALUES ("Conserva");
INSERT INTO categorias(nom_cat) VALUES ("Cereales");

INSERT INTO productos(nom_pro,pre_pro,stock_pro,id_cat_pro) VALUES ("Arroz blanca",3.5,8,3);
INSERT INTO productos(nom_pro,pre_pro,stock_pro,id_cat_pro) VALUES ("Arroz integral",4.5,10,3);
INSERT INTO productos(nom_pro,pre_pro,stock_pro,id_cat_pro) VALUES ("Filete de atun",6,15,2);
INSERT INTO productos(nom_pro,pre_pro,stock_pro,id_cat_pro) VALUES ("Macarrones",1.5,20,1);

INSERT INTO empleados(nom_empleado,apm_empleado,app_empleado,doc_empleado) VALUES ("LuisUno","AmUno","ApUno","123454");
INSERT INTO empleados(nom_empleado,apm_empleado,app_empleado,doc_empleado) VALUES ("LuisDos","AmDos","ApDos","123454");
INSERT INTO empleados(nom_empleado,apm_empleado,app_empleado,doc_empleado) VALUES ("LuisTres","AmTres","ApTres","123454");
INSERT INTO empleados(nom_empleado,apm_empleado,app_empleado,doc_empleado) VALUES ("LuisCuatro","AmCuatro","ApCuatro","123454");
INSERT INTO empleados(nom_empleado,apm_empleado,app_empleado,doc_empleado) VALUES ("LuisCinco","AmCinco","ApCinco","123454");


INSERT INTO facturas(fec_fac,igv_fac,mon_fac,empleado_fac,empresa_fac) VALUES ("2017-01-01",118,100,2,4);
INSERT INTO facturas(fec_fac,igv_fac,mon_fac,empleado_fac,empresa_fac) VALUES ("2017-01-01",238,200,1,1);


INSERT INTO detalle_factura(can_det_fac,fac_det,id_producto) VALUES (10,1,4);
INSERT INTO detalle_factura(can_det_fac,fac_det,id_producto) VALUES (1,1,2);
INSERT INTO detalle_factura(can_det_fac,fac_det,id_producto) VALUES (10,1,3);
INSERT INTO detalle_factura(can_det_fac,fac_det,id_producto) VALUES (10,1,1);

